window.$ = window.jQuery = require("jquery");
require("../../themes/front/dist/assets/vendor/bootstrap/dist/js/bootstrap.min");

window.axios = require('axios');

require('../../themes/front/dist/assets/vendor/hs-toggle-password/dist/js/hs-toggle-password');
require('../../themes/front/src/assets/vendor/hs-navbar-vertical-aside/hs-navbar-vertical-aside.min.js');
require('../../themes/front/src/assets/vendor/hs-mega-menu/dist/hs-mega-menu.min.js');
require('../../themes/front/src/assets/vendor/hs-mega-menu/src/js/hs-mega-menu');
require('../../themes/front/src/assets/js/theme-custom');
require('../../themes/front/dist/assets/js/theme.min');
require('../../themes/front/dist/assets/js/hs.chartjs-matrix');
