require('../bootstrap');
require('./bitboss.theme.config');

window.Vue = require('vue');
window.moment = require('moment');
Vue.prototype.$bus = new Vue();

/** COMPONENTS */
//Reports
Vue.component('reports', require('./components/reports/Reports.vue').default);
Vue.component('star-rating', require('./components/reports/StarRating.vue').default);

import Vue from 'vue';

const app = new Vue({
    el: '#app',
});


/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */