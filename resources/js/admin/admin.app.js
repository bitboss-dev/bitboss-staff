require('../bootstrap');
require('./admin.theme.config');

window.Vue = require('vue');
window.moment = require('moment');
Vue.prototype.$bus = new Vue();

/** COMPONENTS */
//Permits
Vue.component('permits', require('./components/permits/Permits.vue').default);

import Vue from 'vue';
import VCalendar from 'v-calendar';
Vue.use(VCalendar);

const app = new Vue({
    el: '#app',
});


/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */