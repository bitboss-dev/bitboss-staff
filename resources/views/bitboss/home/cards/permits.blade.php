<div class="col mb-3 mb-lg-5">
    <div class="card card-hover-shadow text-center h-100">
        <div class="card-progress-wrap">
            <div class="progress card-progress">
                <div class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>

        <div class="card-body">
            <div class="row align-items-center text-left mb-4">
                <div class="col">
                </div>

                <div class="col-auto">
                    <div class="hs-unfold card-unfold">
                        <a class="js-hs-unfold-invoker btn btn-icon btn-sm btn-ghost-secondary rounded-circle" href="{{route('bitboss.permits.index')}}" data-hs-unfold-options="{
                                   &quot;target&quot;: &quot;#permitsDropdown&quot;,
                                   &quot;type&quot;: &quot;css-animation&quot;
                                 }" data-hs-unfold-target="#permitsDropdown" data-hs-unfold-invoker="">
                            <i class="tio-more-vertical"></i>
                        </a>

                        <div id="permitsDropdown" class="hs-unfold-content dropdown-unfold dropdown-menu dropdown-menu-sm dropdown-menu-right hs-unfold-hidden hs-unfold-content-initialized hs-unfold-css-animation animated hs-unfold-reverse-y"
                             data-hs-target-height="145" data-hs-unfold-content="" data-hs-unfold-content-animation-in="slideInUp" data-hs-unfold-content-animation-out="fadeOut" style="animation-duration: 300ms;">
                            <a class="dropdown-item" href="#">Richiedi un permesso </a>
                            <a class="dropdown-item" href="#">Visualizza permessi</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Opzioni</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="d-flex justify-content-center mb-2">
                <!-- Avatar -->
                <img class="avatar avatar-xl" src="{{asset('images/bitboss/icons/vacation.svg')}}" alt="Image Description">
            </div>

            <div class="mb-4">
                <h2 class="mb-1">Permessi</h2>

                <span class="font-size-sm">{{--Updated 2 hours ago--}}</span>
            </div>

            <small class="card-subtitle">n Membri</small>

            <a class="stretched-link" href="{{route('bitboss.permits.index')}}"></a>
        </div>

        <div class="card-footer">
            <!-- Stats -->
            <div class="row">
                <div class="col">
                    <span class="h4">10</span>
                    <span class="d-block font-size-sm">Numero Richieste</span>
                </div>

                <div class="col column-divider">
                    <span class="h4">33</span>
                    <span class="d-block font-size-sm">Ore Complessive</span>
                </div>

                <div class="col column-divider">
                    <span class="h4">34</span>
                    <span class="d-block font-size-sm">Ore disponibili</span>
                </div>
            </div>
        </div>
    </div>
</div>
</div>