<div class="col mb-3 mb-lg-5">
    <!-- Card -->
    <div class="card card-hover-shadow text-center h-100">
        <div class="card-progress-wrap">
            <!-- Progress -->
            <div class="progress card-progress">
                <div class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <!-- End Progress -->
        </div>

        <!-- Body -->
        <div class="card-body">
            <div class="row align-items-center text-left mb-4">
                <div class="col">
                </div>

                <div class="col-auto">
                    <!-- Unfold -->
                    <div class="hs-unfold card-unfold">
                        <a class="js-hs-unfold-invoker btn btn-icon btn-sm btn-ghost-secondary rounded-circle" href="{{route('bitboss.reports.create')}}" data-hs-unfold-options="{
                                                       &quot;target&quot;: &quot;#reportDropdown&quot;,
                                                       &quot;type&quot;: &quot;css-animation&quot;
                                                     }" data-hs-unfold-target="#reportDropdown" data-hs-unfold-invoker="">
                            <i class="tio-more-vertical"></i>
                        </a>

                        <div id="reportDropdown" class="hs-unfold-content dropdown-unfold dropdown-menu dropdown-menu-sm dropdown-menu-right hs-unfold-hidden hs-unfold-content-initialized hs-unfold-css-animation animated hs-unfold-reverse-y" data-hs-target-height="145" data-hs-unfold-content="" data-hs-unfold-content-animation-in="slideInUp" data-hs-unfold-content-animation-out="fadeOut" style="animation-duration: 300ms;">
                            <a class="dropdown-item" href="#">Scrivi un report </a>
                            <a class="dropdown-item" href="#">Visualizza report</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Opzioni</a>
                        </div>
                    </div>
                    <!-- End Unfold -->
                </div>
            </div>

            <div class="d-flex justify-content-center mb-2">
                <!-- Avatar -->
                <img class="avatar avatar-xl" src="{{asset('images/bitboss/icons/report.svg')}}" alt="Image Description">
            </div>

            <div class="mb-4">
                <h2 class="mb-1">Report</h2>

                <span class="font-size-sm">{{--Updated 2 hours ago--}}</span>
            </div>

            <small class="card-subtitle">n Membri</small>

            <a class="stretched-link" href="{{route('bitboss.reports.create')}}"></a>
        </div>
        <!-- End Body -->

        <!-- Footer -->
        <div class="card-footer">
            <!-- Stats -->
            <div class="row">
                <div class="col">
                    <span class="h4">19</span>
                    <span class="d-block font-size-sm">Punti Totali</span>
                </div>

                <div class="col column-divider">
                    <span class="h4">33</span>
                    <span class="d-block font-size-sm">Partecipanti</span>
                </div>

                <div class="col column-divider">
                    <span class="h4">10</span>
                    <span class="d-block font-size-sm">Giorni Rimasti</span>
                </div>
            </div>
            <!-- End Stats -->
        </div>
        <!-- End Footer -->
    </div>
    <!-- End Card -->
</div>