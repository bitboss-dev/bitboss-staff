@extends('bitboss.app')

@section('content')

    <div class="content container-fluid">
        <div class="row justify-content-lg-center">
            <div class="col-lg-10">
                <!-- Profile Cover -->
                @if($user->cover_image)
                    <div class="profile-cover">
                        <div class="profile-cover-img-wrapper">
                            <img class="profile-cover-img" src="{{$user->cover_image}}" alt="Image Description">
                        </div>
                    </div>
                @else
                    <div class="profile-cover">
                        <div class="profile-cover-img-wrapper">
                            <img class="profile-cover-img" src="{{asset('images/bitboss/cover/cover.png')}}" alt="Image Description">
                        </div>
                    </div>
                @endif
                <!-- End Profile Cover -->

                <!-- Profile Header -->
                <div class="text-center mb-5">
                    @if($user->profile_image)
                        <div class="avatar avatar-xxl avatar-circle avatar-border-lg profile-cover-avatar">
                            <img class="avatar-img" src="{{$user->profile_image}}" alt="Image Description">
                            <span class="avatar-status avatar-status-success"></span>
                        </div>
                    @else
                        <div class="avatar avatar-xxl avatar-circle profile-cover-avatar">
                            <span class="avatar-initials" style="background: #ecf2ff">{{$user->first_name[0]}}</span>
                            <span class="avatar-status avatar-xxl-status avatar-status-success"></span>
                        </div>
                    @endif

                    <h1 class="page-header-title">{{$user->fullName}}
                        @if($user->hasRole('Admin'))
                            <i class="tio-verified tio-lg text-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Top endorsed"></i>
                        @endif



                    </h1>

                    <!-- List -->
                    <ul class="list-inline list-inline-m-1">
                        <li class="list-inline-item">
                            <i class="tio-city mr-1"></i>
                            <span>Mansione</span>
                        </li>


                        <li class="list-inline-item">
                            <i class="tio-date-range mr-1"></i>
                            <span>Joined {{\Carbon\Carbon::parse($user->created_at)->format('M Y')}}</span>
                        </li>
                    </ul>
                    <!-- End List -->
                </div>
                <!-- End Profile Header -->

                <!-- Tab Content -->
                <div class="tab-content" id="projectsTabContent">
                    <div class="tab-pane fade show active" id="grid" role="tabpanel" aria-labelledby="grid-tab">
                        <div class="row row-cols-1 row-cols-md-2">

                            @include('bitboss.home.cards.reports')
                            @include('bitboss.home.cards.permits')

                        </div>
                    </div>
            </div>
        </div>
        <!-- End Row -->
    </div>

@endsection
