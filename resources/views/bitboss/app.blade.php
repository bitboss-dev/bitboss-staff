<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

<!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700&display=swap" rel="stylesheet">

    <!-- Styles -->
    {{--<link href="{{ asset('css/admin.admin.app.css') }}" rel="stylesheet" defer>--}}
    <link href="{{ mix('/css/bitboss.app.css') }}" rel="stylesheet" defer>

    @yield('style')
</head>

<body class="footer-offset footer-offset has-navbar-vertical-aside">

@include('bitboss.partials.header')

<main id="content" role="main" class="main pointer-event">
<!-- Content -->
    <div class="p-3 NOcontent NOcontainer-fluid" id="app">
        @yield('content')
    </div> <!-- content -->

</main>

</div>

<script src="{{ mix('/js/bitboss.app.js') }}"></script>
<script src="{{ asset('/js/vendor/hs-unfold/dist/hs-unfold.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.7.3/dist/alpine.min.js" defer></script>

<script>

    document.addEventListener('DOMContentLoaded', function (event) {

        @if (session()->has('success'))
        window.showSuccessToast('{{session()->get('success')}}');
        @endif
        @if (session()->has('error'))
        window.showErrorToast('{{session()->get('error')}}');
        @endif

        var sidebar = $('.js-navbar-vertical-aside').hsSideNav();

        $('.js-hs-unfold-invoker').each(function () {
            var unfold = new HSUnfold($(this)).init();
        });

        $('div.alert').not('.alert-important').delay(4000).fadeOut(350);
    });

</script>

@stack('scripts_bottom')

</body>
</html>
