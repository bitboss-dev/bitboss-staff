@extends('bitboss.app', ['title'=>__("Segnalazioni")])

@section('content')

    <div class="mb-7">
        <div class="content container-fluid col-10">

            <div class="card mb-3 mb-lg-5">
                <!-- Body -->
                <div class="card-body">
                    <div class="row align-items-md-center gx-md-5">
                        <div class="col-md-auto mb-3 mb-md-0">
                            <div class="d-flex align-items-center">
                                <img class="avatar avatar-xxl avatar-4by3 mr-4" src="{{asset('images/bitboss/reports/5-stars.svg')}}" alt="Image Description">

                                <div class="d-block">
                                    <h4 class="display-2 text-dark mb-0">23</h4>
                                    <p><span class="badge badge-soft-dark badge-pill ml-1">+1 ultimo mese</span></p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md">
                            <ul class="list-unstyled list-unstyled-py-2 mb-0">
                                <!-- Review Ratings -->
                                <li class="d-flex align-items-center font-size-sm">
                                    <span class="mr-3">5 stelle</span>
                                    <div class="progress flex-grow-1">
                                        <div class="progress-bar" role="progressbar" style="width: 82%;" aria-valuenow="82" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <span class="ml-3">4</span>
                                </li>
                                <!-- End Review Ratings -->

                                <!-- Review Ratings -->
                                <li class="d-flex align-items-center font-size-sm">
                                    <span class="mr-3">4 stelle</span>
                                    <div class="progress flex-grow-1">
                                        <div class="progress-bar" role="progressbar" style="width: 18%;" aria-valuenow="18" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <span class="ml-3">3</span>
                                </li>
                                <!-- End Review Ratings -->

                                <!-- Review Ratings -->
                                <li class="d-flex align-items-center font-size-sm">
                                    <span class="mr-3">3 stelle</span>
                                    <div class="progress flex-grow-1">
                                        <div class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <span class="ml-3">0</span>
                                </li>
                                <!-- End Review Ratings -->

                                <!-- Review Ratings -->
                                <li class="d-flex align-items-center font-size-sm">
                                    <span class="mr-3">2 stelle</span>
                                    <div class="progress flex-grow-1">
                                        <div class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <span class="ml-3">0</span>
                                </li>
                                <!-- End Review Ratings -->

                                <!-- Review Ratings -->
                                <li class="d-flex align-items-center font-size-sm">
                                    <span class="mr-3">1 stella</span>
                                    <div class="progress flex-grow-1">
                                        <div class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <span class="ml-3">0</span>
                                </li>
                                <!-- End Review Ratings -->
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Body -->
            </div>

            <reports :users-prop="{{$users}}" upload-url-prop="{{route('bitboss.upload')}}" csrf="{{csrf_token()}}" v-cloak></reports>
        </div>
    </div>

@endsection