@extends('bitboss.app', ['title'=>__("Permessi")])

@section('content')

    <div class="mb-7">
        <div class="content container-fluid">
            <!-- Page Header -->
            <div class="page-header">
                <div class="row align-items-end">
                    <div class="col-sm mb-2 mb-sm-0">

                        <h1 class="page-header-title">Permessi</h1>
                    </div>
                </div>
                <!-- End Row -->
            </div>
            <!-- End Page Header -->

            </div>
        </div>
    </div>

@endsection