@if (session()->has('message'))
    <div class="m-2">
        <div class="alert alert-success ">
            {{ session('message') }}
        </div>
    </div>
@endif

@if (session()->has('error'))
    <div class="m-2">
        <div class="alert alert-danger ">
            {{ session('error') }}
        </div>
    </div>
@endif

