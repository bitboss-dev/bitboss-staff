<aside class="js-navbar-vertical-aside navbar navbar-vertical-aside navbar-vertical navbar-vertical-fixed navbar-expand-xl navbar-bordered default navbar-vertical-aside-initialized">
    <div class="navbar-vertical-container">
        <div class="navbar-vertical-footer-offset">
            <div class="navbar-brand-wrapper justify-content-between">
                <a class="navbar-brand" href="{{route('admin.dashboard')}}" aria-label="Front">
                    <img class="img-fluid" src="{{asset('images/logo/logo.png')}}" alt="Logo">
                    <img class="navbar-brand-logo-mini" src="../assets/svg/logos/logo-short.svg" alt="Logo">
                </a>
            </div>

            <!-- Content -->
            <div class="navbar-vertical-content mt-4">
                <ul class="navbar-nav navbar-nav-lg nav-tabs">
                    <li class="navbar-vertical-aside-has-menu">
                        <a class="js-navbar-vertical-aside-menu-link nav-link" href="{{route('admin.dashboard')}}" title="Dashboards">
                            <i class="tio-home nav-icon"></i>
                            <span class="navbar-vertical-aside-mini-mode-hidden-elements text-truncate spinner-on-click">Dashboard</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <small class="nav-subtitle" title="Layouts">Team</small>
                        <small class="tio-more-horizontal nav-subtitle-replacer"></small>
                    </li>

                    <li class="navbar-vertical-aside-has-menu">
                        <a class="js-navbar-vertical-aside-menu-link nav-link" href="{{route('admin.permits.index')}}" title="Permessi">
                            <i class="tio-event nav-icon"></i>
                            <span class="navbar-vertical-aside-mini-mode-hidden-elements text-truncate spinner-on-click">Permessi</span>
                        </a>
                    </li>

                    <li class="navbar-vertical-aside-has-menu">
                        <a class="js-navbar-vertical-aside-menu-link nav-link" href="{{route('admin.reports.index')}}" title="Reports">
                            <i class="tio-report nav-icon"></i>
                            <span class="navbar-vertical-aside-mini-mode-hidden-elements text-truncate spinner-on-click">Segnalazioni</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <small class="nav-subtitle" title="Layouts">Bitboss Ticket</small>
                        <small class="tio-more-horizontal nav-subtitle-replacer"></small>
                    </li>

                    <li class="navbar-vertical-aside-has-menu">
                        <a class="js-navbar-vertical-aside-menu-link nav-link" href="#" title="Ticket">
                            <i class="tio-headphones nav-icon"></i>
                            <span class="navbar-vertical-aside-mini-mode-hidden-elements text-truncate spinner-on-click">Ticket</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <small class="nav-subtitle" title="Layouts">Amministrazione</small>
                        <small class="tio-more-horizontal nav-subtitle-replacer"></small>
                    </li>

                    <li class="navbar-vertical-aside-has-menu">
                        <a class="js-navbar-vertical-aside-menu-link nav-link" href="{{route('admin.users.index')}}" title="Utenti">
                            <i class="tio-user nav-icon"></i>
                            <span class="navbar-vertical-aside-mini-mode-hidden-elements text-truncate spinner-on-click">Utenti</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- End Content -->
        </div>
    </div>
</aside>