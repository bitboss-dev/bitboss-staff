<header id="header"
        class="navbar navbar-expand-lg navbar-fixed navbar-height navbar-flush navbar-container navbar-bordered">
    <div class="navbar-nav-wrap">
        <div class="navbar-brand-wrapper">
            <!-- Logo -->
            <a class="navbar-brand" href="{{route('admin.dashboard')}}" aria-label="Front">
                <img class="navbar-brand-logo d-none d-xl-block img-fluid" src="/images/logo/logo.png" alt="Logo">
                <img class="small-navbar-brand-logo d-xl-none my-auto img-fluid" src="/images/logo/logo.png" alt="Logo">
            </a>
            <!-- End Logo -->
        </div>

        <div class="navbar-nav-wrap-content-left">
        </div>

        <!-- Secondary Content -->
        <div class="navbar-nav-wrap-content-right">

            <!-- Navbar -->
            <ul class="navbar-nav align-items-center flex-row">
                <li class="nav-item d-none d-sm-inline-block">
                    <div class="hs-unfold">
                        <a class="js-hs-unfold-invoker btn btn-icon btn-ghost-secondary rounded-circle" href="javascript:;"
                           data-hs-unfold-options='{"target": "#notificationDropdown","type": "css-animation"}'>
                            <i class="tio-notifications-on-outlined"></i>
                            @if(count(\Illuminate\Support\Facades\Auth::user()->unreadNotifications))
                                <span class="btn-status btn-sm-status btn-status-danger"></span>
                            @endif
                        </a>

                        <div id="notificationDropdown" class="m-0 hs-unfold-content dropdown-unfold dropdown-menu dropdown-menu-right navbar-dropdown-menu hs-unfold-css-animation animated slideInUp" style="width: 25rem; animation-duration: 300ms;" data-hs-target-height="518" data-hs-unfold-content="" data-hs-unfold-content-animation-in="slideInUp" data-hs-unfold-content-animation-out="fadeOut" >
                            <!-- Header -->
                            <div class="card-header">
                                <span class="card-title h4">Notifiche</span>

                                <!-- Unfold -->
                                <div class="hs-unfold">
                                    <a class="js-hs-unfold-invoker btn btn-icon btn-sm btn-ghost-secondary rounded-circle" href="javascript:;" data-hs-unfold-options="{
                       &quot;target&quot;: &quot;#notificationSettingsOneDropdown&quot;,
                       &quot;type&quot;: &quot;css-animation&quot;
                     }" data-hs-unfold-target="#notificationSettingsOneDropdown" data-hs-unfold-invoker="">
                                        <i class="tio-more-vertical"></i>
                                    </a>
                                    <div id="notificationSettingsOneDropdown" class="hs-unfold-content dropdown-unfold dropdown-menu dropdown-menu-right hs-unfold-hidden hs-unfold-content-initialized hs-unfold-css-animation animated" data-hs-target-height="0" data-hs-unfold-content="" data-hs-unfold-content-animation-in="slideInUp" data-hs-unfold-content-animation-out="fadeOut" style="animation-duration: 300ms;">
                                        <span class="dropdown-header">Impostazioni</span>
                                        {{--<a class="dropdown-item" href="#">
                                            <i class="tio-archive dropdown-item-icon"></i>
                                            Archivia tutti
                                        </a>--}}
                                        <a class="dropdown-item" href="{{route('admin.notifications.readAll')}}">
                                            <i class="tio-all-done dropdown-item-icon"></i>
                                            Segna tutti come letto
                                        </a>
                                        {{--<a class="dropdown-item" href="#">
                                            <i class="tio-toggle-off dropdown-item-icon"></i>
                                            Disable notifications
                                        </a>--}}
                                        {{--<div class="dropdown-divider"></div>
                                        <span class="dropdown-header">Feedback</span>
                                        <a class="dropdown-item" href="#">
                                            <i class="tio-chat-outlined dropdown-item-icon"></i>
                                            Report
                                        </a>--}}
                                    </div>
                                </div>
                                <!-- End Unfold -->
                            </div>
                            <!-- End Header -->

                            <!-- Nav -->
                            <ul class="nav nav-tabs nav-justified" id="notificationTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="notificationNavOne-tab" data-toggle="tab" href="#notificationNavOne" role="tab" aria-controls="notificationNavOne" aria-selected="true">Notifiche ({{count(\Illuminate\Support\Facades\Auth::user()->unreadNotifications)}})</a>
                                </li>
                                {{--<li class="nav-item">
                                    <a class="nav-link" id="notificationNavTwo-tab" data-toggle="tab" href="#notificationNavTwo" role="tab" aria-controls="notificationNavTwo" aria-selected="false">Archiviate</a>
                                </li>--}}
                            </ul>
                            <!-- End Nav -->

                            <!-- Body -->
                            <div class="card-body-height">
                                <!-- Tab Content -->
                                <div class="tab-content" id="notificationTabContent">
                                    <div class="tab-pane fade show active" id="notificationNavOne" role="tabpanel" aria-labelledby="notificationNavOne-tab">
                                        <ul class="list-group list-group-flush navbar-card-list-group">
                                            @foreach(\Illuminate\Support\Facades\Auth::user()->notifications as $notification)
                                                <li class="list-group-item custom-checkbox-list-wrapper">
                                                    <div class="row">
                                                        <div class="col-auto position-static">
                                                            <div class="d-flex align-items-center">
                                                                <div class="custom-control custom-checkbox custom-checkbox-list">
                                                                    @if (!$notification->read_at)
                                                                        <input type="checkbox" class="custom-control-input" id="notificationCheck1" checked="">
                                                                    @endif
                                                                    <label class="custom-control-label" for="notificationCheck1"></label>
                                                                    <span class="custom-checkbox-list-stretched-bg"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col ml-n3">
                                                            <span class="card-title h5">{{@$notification->data['title']}}</span>
                                                            <p class="card-text font-size-sm">{{@$notification->data['text']}}</p>
                                                        </div>
                                                        <small class="col-auto text-muted text-cap">
                                                            @if (\Carbon\Carbon::parse($notification->created_at)->diffInSeconds(\Carbon\Carbon::now()) < 60)
                                                                {{\Carbon\Carbon::parse($notification->created_at)->diffInSeconds(\Carbon\Carbon::now())}} sec
                                                            @elseif(\Carbon\Carbon::parse($notification->created_at)->diffInMinutes(\Carbon\Carbon::now()) < 60)
                                                                {{\Carbon\Carbon::parse($notification->created_at)->diffInMinutes(\Carbon\Carbon::now())}} min
                                                            @elseif(\Carbon\Carbon::parse($notification->created_at)->diffInHours(\Carbon\Carbon::now()) < 24)
                                                                {{\Carbon\Carbon::parse($notification->created_at)->diffInHours(\Carbon\Carbon::now())}} hr
                                                            @else
                                                                {{\Carbon\Carbon::parse($notification->created_at)->diffInDays(\Carbon\Carbon::now())}} gg
                                                            @endif
                                                        </small>
                                                    </div>
                                                    <a class="stretched-link" href="{{route('admin.notifications.read', ['notification' => $notification])}}"></a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>

                                    {{--<div class="tab-pane fade" id="notificationNavTwo" role="tabpanel" aria-labelledby="notificationNavTwo-tab">
                                        <ul class="list-group list-group-flush navbar-card-list-group">
                                            <!-- Item -->
                                            <li class="list-group-item custom-checkbox-list-wrapper">
                                                <div class="row">
                                                    <div class="col-auto position-static">
                                                        <div class="d-flex align-items-center">
                                                            <div class="custom-control custom-checkbox custom-checkbox-list">
                                                                <input type="checkbox" class="custom-control-input" id="notificationCheck7">
                                                                <label class="custom-control-label" for="notificationCheck7"></label>
                                                                <span class="custom-checkbox-list-stretched-bg"></span>
                                                            </div>
                                                            <div class="avatar avatar-sm avatar-soft-dark avatar-circle">
                                                                <span class="avatar-initials">A</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col ml-n3">
                                                        <span class="card-title h5">Anne Richard</span>
                                                        <p class="card-text font-size-sm">accepted your invitation to join Notion</p>
                                                    </div>
                                                    <small class="col-auto text-muted text-cap">1dy</small>
                                                </div>
                                                <a class="stretched-link" href="#"></a>
                                            </li>
                                            <!-- End Item -->

                                            <!-- Item -->
                                            <li class="list-group-item custom-checkbox-list-wrapper">
                                                <div class="row">
                                                    <div class="col-auto position-static">
                                                        <div class="d-flex align-items-center">
                                                            <div class="custom-control custom-checkbox custom-checkbox-list">
                                                                <input type="checkbox" class="custom-control-input" id="notificationCheck6">
                                                                <label class="custom-control-label" for="notificationCheck6"></label>
                                                                <span class="custom-checkbox-list-stretched-bg"></span>
                                                            </div>
                                                            <div class="avatar avatar-sm avatar-circle">
                                                                <img class="avatar-img" src="./assets/img/160x160/img5.jpg" alt="Image Description">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col ml-n3">
                                                        <span class="card-title h5">Finch Hoot</span>
                                                        <p class="card-text font-size-sm">left Slack group HS projects</p>
                                                    </div>
                                                    <small class="col-auto text-muted text-cap">3dy</small>
                                                </div>
                                                <a class="stretched-link" href="#"></a>
                                            </li>
                                            <!-- End Item -->

                                            <!-- Item -->
                                            <li class="list-group-item custom-checkbox-list-wrapper">
                                                <div class="row">
                                                    <div class="col-auto position-static">
                                                        <div class="d-flex align-items-center">
                                                            <div class="custom-control custom-checkbox custom-checkbox-list">
                                                                <input type="checkbox" class="custom-control-input" id="notificationCheck8">
                                                                <label class="custom-control-label" for="notificationCheck8"></label>
                                                                <span class="custom-checkbox-list-stretched-bg"></span>
                                                            </div>
                                                            <div class="avatar avatar-sm avatar-dark avatar-circle">
                                                                <span class="avatar-initials">HS</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col ml-n3">
                                                        <span class="card-title h5">Htmlstream</span>
                                                        <p class="card-text font-size-sm">you earned a "Top endorsed" <i class="tio-verified text-primary"></i> badge</p>
                                                    </div>
                                                    <small class="col-auto text-muted text-cap">6dy</small>
                                                </div>
                                                <a class="stretched-link" href="#"></a>
                                            </li>
                                            <!-- End Item -->

                                            <!-- Item -->
                                            <li class="list-group-item custom-checkbox-list-wrapper">
                                                <div class="row">
                                                    <div class="col-auto position-static">
                                                        <div class="d-flex align-items-center">
                                                            <div class="custom-control custom-checkbox custom-checkbox-list">
                                                                <input type="checkbox" class="custom-control-input" id="notificationCheck9">
                                                                <label class="custom-control-label" for="notificationCheck9"></label>
                                                                <span class="custom-checkbox-list-stretched-bg"></span>
                                                            </div>
                                                            <div class="avatar avatar-sm avatar-circle">
                                                                <img class="avatar-img" src="./assets/img/160x160/img8.jpg" alt="Image Description">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col ml-n3">
                                                        <span class="card-title h5">Linda Bates</span>
                                                        <p class="card-text font-size-sm">Accepted your connection</p>
                                                    </div>
                                                    <small class="col-auto text-muted text-cap">17dy</small>
                                                </div>
                                                <a class="stretched-link" href="#"></a>
                                            </li>
                                            <!-- End Item -->

                                            <!-- Item -->
                                            <li class="list-group-item custom-checkbox-list-wrapper">
                                                <div class="row">
                                                    <div class="col-auto position-static">
                                                        <div class="d-flex align-items-center">
                                                            <div class="custom-control custom-checkbox custom-checkbox-list">
                                                                <input type="checkbox" class="custom-control-input" id="notificationCheck10">
                                                                <label class="custom-control-label" for="notificationCheck10"></label>
                                                                <span class="custom-checkbox-list-stretched-bg"></span>
                                                            </div>
                                                            <div class="avatar avatar-sm avatar-soft-dark avatar-circle">
                                                                <span class="avatar-initials">L</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col ml-n3">
                                                        <span class="card-title h5">Lewis Clarke</span>
                                                        <p class="card-text font-size-sm">completed <i class="tio-folder-bookmarked text-primary"></i> FD-134 task</p>
                                                    </div>
                                                    <small class="col-auto text-muted text-cap">2mn</small>
                                                </div>
                                                <a class="stretched-link" href="#"></a>
                                            </li>
                                            <!-- End Item -->
                                        </ul>
                                    </div>--}}
                                </div>
                                <!-- End Tab Content -->
                            </div>
                            <!-- End Body -->

                            <!-- Card Footer -->
                            <a class="card-footer text-center" href="#">
                                Visualizza tutte le notifiche
                                <i class="tio-chevron-right"></i>
                            </a>
                            <!-- End Card Footer -->
                        </div>
                    </div>
                    <!-- End Notification -->
                </li>
                <li class="nav-item">
                    <!-- Account -->
                    <div class="hs-unfold">
                        <a class="js-hs-unfold-invoker navbar-dropdown-account-wrapper" href="#"
                           data-hs-unfold-options='{"target": "#accountNavbarDropdown","type": "css-animation"}'
                           data-hs-unfold-target="#accountNavbarDropdown" data-hs-unfold-invoker="">
                            <div class="avatar avatar-sm avatar-circle">
                                <div class="avatar avatar-sm avatar-soft-primary avatar-circle mr-2">
                                    <span class="avatar-initials">{{\Illuminate\Support\Facades\Auth::user()->first_name[0]}}</span>
                                </div>
                                <span class="avatar-status avatar-sm-status avatar-status-success"></span>
                            </div>
                        </a>

                        <div id="accountNavbarDropdown"
                             class="hs-unfold-content dropdown-unfold dropdown-menu dropdown-menu-right navbar-dropdown-menu navbar-dropdown-account hs-unfold-hidden hs-unfold-content-initialized hs-unfold-css-animation animated"
                             style="width: 16rem; animation-duration: 300ms;" data-hs-target-height="394.281"
                             data-hs-unfold-content="" data-hs-unfold-content-animation-in="slideInUp"
                             data-hs-unfold-content-animation-out="fadeOut">
                            <div class="dropdown-item">
                                <div class="media align-items-center">
                                    <div class="avatar avatar-sm avatar-circle mr-2">
                                        <span class="avatar-initials">{{\Illuminate\Support\Facades\Auth::user()->first_name[0]}}</span>
                                    </div>
                                    <div class="media-body">
                                        <span class="card-title h5">{{\Illuminate\Support\Facades\Auth::user()->fullName}}</span>
                                        <span class="card-text">{{\Illuminate\Support\Facades\Auth::user()->email}}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="dropdown-divider"></div>

                            <a class="dropdown-item" href="#">
                                <form method="POST" action="{{route('logout')}}">
                                    {!! csrf_field() !!}
                                    <button class="btn btn-link  font-weight-normal text-truncate pr-2" type="submit">
                                        Logout
                                    </button>
                                </form>
                            </a>
                        </div>
                    </div>
                    <!-- End Account -->
                </li>
            </ul>
            <!-- End Navbar -->
        </div>
        <!-- End Secondary Content -->
    </div>
</header>