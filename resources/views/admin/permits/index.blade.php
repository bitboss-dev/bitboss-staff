@extends('admin.app', ['title'=>__("Permessi")])

@section('content')

    <div class="mb-7">
        <div class="content container-fluid">
            <!-- Page Header -->
            <div class="page-header">
                <div class="row align-items-end">
                    <div class="col-sm mb-2 mb-sm-0">

                        <h1 class="page-header-title">Permessi</h1>
                    </div>

                    <div class="col-sm-auto">
                        {{--<a class="btn btn-primary" href="{{route('admin.users.create')}}">
                            <i class="tio-user-add mr-1"></i> Nuovo
                        </a>--}}
                    </div>
                </div>
                <!-- End Row -->
            </div>
            <!-- End Page Header -->

            <div class="card">
                <div class="card-body">
                    <permits :permits-prop="{{json_encode($permits)}}" v-cloak></permits>
                </div>
            </div>

            </div>
        </div>
    </div>

@endsection