@extends('admin.app', ['title'=>__("Permessi")])

@section('content')

    <div class="mb-7">
        <div class="content container-fluid">
            <!-- Page Header -->
            <div class="page-header">
                <div class="row align-items-end">
                    <div class="col-sm mb-2 mb-sm-0">

                        <h1 class="page-header-title">Report</h1>
                    </div>

                    <div class="col-sm-auto">
                    </div>
                </div>
                <!-- End Row -->
            </div>
            <!-- End Page Header -->

            <div class="card">
                <div class="card-body">
                    <div class="table-responsive datatable-custom">
                        <div id="datatable_wrapper" class="dataTables_wrapper no-footer"><div class="dt-buttons">          <button class="dt-button buttons-copy buttons-html5 d-none" tabindex="0" aria-controls="datatable" type="button"><span>Copy</span></button> <button class="dt-button buttons-excel buttons-html5 d-none" tabindex="0" aria-controls="datatable" type="button"><span>Excel</span></button> <button class="dt-button buttons-csv buttons-html5 d-none" tabindex="0" aria-controls="datatable" type="button"><span>CSV</span></button> <button class="dt-button buttons-pdf buttons-html5 d-none" tabindex="0" aria-controls="datatable" type="button"><span>PDF</span></button> <button class="dt-button buttons-print d-none" tabindex="0" aria-controls="datatable" type="button"><span>Print</span></button> </div><div id="datatable_filter" class="dataTables_filter"><label>Search:<input type="search" class="" placeholder="" aria-controls="datatable"></label></div><table id="datatable" class="table table-borderless table-thead-bordered table-nowrap card-table dataTable no-footer" data-hs-datatables-options="{
                     &quot;columnDefs&quot;: [{
                        &quot;targets&quot;: [0, 3, 6],
                        &quot;orderable&quot;: false
                      }],
                     &quot;order&quot;: [],
                     &quot;info&quot;: {
                       &quot;totalQty&quot;: &quot;#datatableWithPaginationInfoTotalQty&quot;
                     },
                     &quot;search&quot;: &quot;#datatableSearch&quot;,
                     &quot;entries&quot;: &quot;#datatableEntries&quot;,
                     &quot;pageLength&quot;: 5,
                     &quot;isResponsive&quot;: false,
                     &quot;isShowPaging&quot;: false,
                     &quot;pagination&quot;: &quot;datatablePagination&quot;
                   }" role="grid" aria-describedby="datatable_info">
                                <thead class="thead-light">
                                <tr role="row"><th scope="col" class="table-column-pr-0 sorting_disabled" rowspan="1" colspan="1" aria-label="




                  " style="width: 24px;">
                                        <div class="custom-control custom-checkbox">
                                            <input id="datatableCheckAll" type="checkbox" class="custom-control-input">
                                            <label class="custom-control-label" for="datatableCheckAll"></label>
                                        </div>
                                    </th><th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Product: activate to sort column ascending" style="width: 196px;">Product</th><th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Reviewer: activate to sort column ascending" style="width: 201px;">Reviewer</th><th class="sorting_disabled" rowspan="1" colspan="1" aria-label="Review" style="width: 288px;">Review</th><th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Date: activate to sort column ascending" style="width: 131px;">Date</th><th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 69px;">Status</th><th class="sorting_disabled" rowspan="1" colspan="1" aria-label="Actions" style="width: 52px;">Actions</th></tr>
                                </thead>

                                <tbody>













                                <tr role="row" class="odd">
                                    <td class="table-column-pr-0">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="usersDataCheck2">
                                            <label class="custom-control-label" for="usersDataCheck2"></label>
                                        </div>
                                    </td>
                                    <th>
                                        <a class="d-flex align-items-center" href="./user-profile.html">
                                            <div class="avatar">
                                                <img class="avatar-img" src="./assets/img/400x400/img18.jpg" alt="Image Description">
                                            </div>
                                            <div class="ml-3">
                                                <span class="h5 text-hover-primary">Brown Hat</span>
                                            </div>
                                        </a>
                                    </th>
                                    <td>
                                        <a class="d-flex align-items-center" href="./user-profile.html">
                                            <div class="avatar avatar-circle">
                                                <img class="avatar-img" src="./assets/img/160x160/img10.jpg" alt="Image Description">
                                            </div>
                                            <div class="ml-3">
                                                <span class="d-block h5 text-hover-primary mb-0">Amanda Harvey <i class="tio-verified text-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Verified purchase"></i></span>
                                                <span class="d-block font-size-sm text-body">amanda@example.com</span>
                                            </div>
                                        </a>
                                    </td>
                                    <td>
                                        <div class="text-wrap" style="width: 18rem;">
                                            <div class="d-flex mb-2">
                                                <div class="mr-1"><img src="./assets/svg/components/star.svg" alt="Review rating" width="14"></div>
                                                <div class="mr-1"><img src="./assets/svg/components/star.svg" alt="Review rating" width="14"></div>
                                                <div class="mr-1"><img src="./assets/svg/components/star.svg" alt="Review rating" width="14"></div>
                                                <div class="mr-1"><img src="./assets/svg/components/star.svg" alt="Review rating" width="14"></div>
                                                <div class="mr-1"><img src="./assets/svg/components/star.svg" alt="Review rating" width="14"></div>
                                            </div>

                                            <h4 class="mb-1">I just love it!</h4>
                                            <p>I bought this hat for my boyfriend, but then i found out he cheated on me so I kept it and I love it!! I wear it all the time and there is no problem with the fit even though its a mens" hat.</p>
                                        </div>
                                    </td>
                                    <td>Aug 17, 2020, 5:48</td>
                                    <td><span class="badge badge-soft-success ml-2">Published</span></td>
                                    <td>
                                        <!-- Unfold -->
                                        <div class="hs-unfold">
                                            <a class="js-hs-unfold-invoker btn btn-icon btn-sm btn-ghost-secondary rounded-circle" href="javascript:;" data-hs-unfold-options="{
                           &quot;target&quot;: &quot;#settingsDropdown1&quot;,
                           &quot;type&quot;: &quot;css-animation&quot;
                         }" data-hs-unfold-target="#settingsDropdown1" data-hs-unfold-invoker="">
                                                <i class="tio-more-horizontal"></i>
                                            </a>

                                            <div id="settingsDropdown1" class="hs-unfold-content dropdown-unfold dropdown-menu dropdown-menu-right mt-1 hs-unfold-hidden hs-unfold-content-initialized hs-unfold-css-animation animated hs-unfold-reverse-y" data-hs-target-height="243" data-hs-unfold-content="" data-hs-unfold-content-animation-in="slideInUp" data-hs-unfold-content-animation-out="fadeOut" style="animation-duration: 300ms;">
                                                <span class="dropdown-header">Settings</span>

                                                <a class="dropdown-item" href="#">
                                                    <i class="tio-edit dropdown-item-icon"></i> Edit
                                                </a>
                                                <a class="dropdown-item" href="#">
                                                    <i class="tio-publish dropdown-item-icon"></i> Publish
                                                </a>
                                                <a class="dropdown-item" href="#">
                                                    <i class="tio-delete-outlined dropdown-item-icon"></i> Delete
                                                </a>

                                                <div class="dropdown-divider"></div>

                                                <span class="dropdown-header">Feedback</span>

                                                <a class="dropdown-item" href="#">
                                                    <i class="tio-reply dropdown-item-icon"></i> Reply
                                                </a>
                                            </div>
                                        </div>
                                        <!-- End Unfold -->
                                    </td>
                                </tr><tr role="row" class="even">
                                    <td class="table-column-pr-0">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="usersDataCheck3">
                                            <label class="custom-control-label" for="usersDataCheck3"></label>
                                        </div>
                                    </td>
                                    <th>
                                        <a class="d-flex align-items-center" href="./user-profile.html">
                                            <div class="avatar">
                                                <img class="avatar-img" src="./assets/img/400x400/img3.jpg" alt="Image Description">
                                            </div>
                                            <div class="ml-3">
                                                <span class="h5 text-hover-primary">Calvin Klein t-shirts</span>
                                            </div>
                                        </a>
                                    </th>
                                    <td>
                                        <a class="d-flex align-items-center" href="./user-profile.html">
                                            <div class="avatar avatar-soft-dark avatar-circle">
                                                <span class="avatar-initials">A</span>
                                            </div>
                                            <div class="ml-3">
                                                <span class="d-block h5 text-hover-primary mb-0">Anne Richard</span>
                                                <span class="d-block font-size-sm text-body">anne@example.com</span>
                                            </div>
                                        </a>
                                    </td>
                                    <td>
                                        <div class="text-wrap" style="width: 18rem;">
                                            <div class="d-flex mb-2">
                                                <div class="mr-1"><img src="./assets/svg/components/star.svg" alt="Review rating" width="14"></div>
                                                <div class="mr-1"><img src="./assets/svg/components/star.svg" alt="Review rating" width="14"></div>
                                                <div class="mr-1"><img src="./assets/svg/components/star.svg" alt="Review rating" width="14"></div>
                                                <div class="mr-1"><img src="./assets/svg/components/star.svg" alt="Review rating" width="14"></div>
                                                <div class="mr-1"><img src="./assets/svg/components/star.svg" alt="Review rating" width="14"></div>
                                            </div>

                                            <h4 class="mb-1">Really nice</h4>
                                            <p>Material is great and very comfortable and stylish.</p>
                                        </div>
                                    </td>
                                    <td>Aug 04, 2020, 3:17</td>
                                    <td><span class="badge badge-soft-warning ml-2">Pending</span></td>
                                    <td>
                                        <!-- Unfold -->
                                        <div class="hs-unfold">
                                            <a class="js-hs-unfold-invoker btn btn-icon btn-sm btn-ghost-secondary rounded-circle" href="javascript:;" data-hs-unfold-options="{
                           &quot;target&quot;: &quot;#settingsDropdown2&quot;,
                           &quot;type&quot;: &quot;css-animation&quot;
                         }" data-hs-unfold-target="#settingsDropdown2" data-hs-unfold-invoker="">
                                                <i class="tio-more-horizontal"></i>
                                            </a>

                                            <div id="settingsDropdown2" class="hs-unfold-content dropdown-unfold dropdown-menu dropdown-menu-right mt-1 hs-unfold-hidden hs-unfold-content-initialized hs-unfold-css-animation animated hs-unfold-reverse-y" data-hs-target-height="243" data-hs-unfold-content="" data-hs-unfold-content-animation-in="slideInUp" data-hs-unfold-content-animation-out="fadeOut" style="animation-duration: 300ms;">
                                                <span class="dropdown-header">Settings</span>

                                                <a class="dropdown-item" href="#">
                                                    <i class="tio-edit dropdown-item-icon"></i> Edit
                                                </a>
                                                <a class="dropdown-item" href="#">
                                                    <i class="tio-publish dropdown-item-icon"></i> Publish
                                                </a>
                                                <a class="dropdown-item" href="#">
                                                    <i class="tio-delete-outlined dropdown-item-icon"></i> Delete
                                                </a>

                                                <div class="dropdown-divider"></div>

                                                <span class="dropdown-header">Feedback</span>

                                                <a class="dropdown-item" href="#">
                                                    <i class="tio-reply dropdown-item-icon"></i> Reply
                                                </a>
                                            </div>
                                        </div>
                                        <!-- End Unfold -->
                                    </td>
                                </tr><tr role="row" class="odd">
                                    <td class="table-column-pr-0">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="usersDataCheck4">
                                            <label class="custom-control-label" for="usersDataCheck4"></label>
                                        </div>
                                    </td>
                                    <th>
                                        <a class="d-flex align-items-center" href="./user-profile.html">
                                            <div class="avatar">
                                                <img class="avatar-img" src="./assets/img/400x400/img24.jpg" alt="Image Description">
                                            </div>
                                            <div class="ml-3">
                                                <span class="h5 text-hover-primary">Clarks shoes</span>
                                            </div>
                                        </a>
                                    </th>
                                    <td>
                                        <a class="d-flex align-items-center" href="./user-profile.html">
                                            <div class="avatar avatar-circle">
                                                <img class="avatar-img" src="./assets/img/160x160/img3.jpg" alt="Image Description">
                                            </div>
                                            <div class="ml-3">
                                                <span class="d-block h5 text-hover-primary mb-0">David Harrison</span>
                                                <span class="d-block font-size-sm text-body">david@example.com</span>
                                            </div>
                                        </a>
                                    </td>
                                    <td>
                                        <div class="text-wrap" style="width: 18rem;">
                                            <div class="d-flex mb-2">
                                                <div class="mr-1"><img src="./assets/svg/components/star.svg" alt="Review rating" width="14"></div>
                                                <div class="mr-1"><img src="./assets/svg/components/star.svg" alt="Review rating" width="14"></div>
                                                <div class="mr-1"><img src="./assets/svg/components/star.svg" alt="Review rating" width="14"></div>
                                                <div class="mr-1"><img src="./assets/svg/components/star.svg" alt="Review rating" width="14"></div>
                                                <div class="mr-1"><img src="./assets/svg/components/star-muted.svg" alt="Review rating" width="14"></div>
                                            </div>

                                            <h4 class="mb-1">Good product</h4>
                                            <p>A really well built shoe. It looks great and wears just as well. A great staple in ball caps.</p>
                                        </div>
                                    </td>
                                    <td>June 18, 2020, 09:19</td>
                                    <td><span class="badge badge-soft-success ml-2">Published</span></td>
                                    <td>
                                        <!-- Unfold -->
                                        <div class="hs-unfold">
                                            <a class="js-hs-unfold-invoker btn btn-icon btn-sm btn-ghost-secondary rounded-circle" href="javascript:;" data-hs-unfold-options="{
                           &quot;target&quot;: &quot;#settingsDropdown3&quot;,
                           &quot;type&quot;: &quot;css-animation&quot;
                         }" data-hs-unfold-target="#settingsDropdown3" data-hs-unfold-invoker="">
                                                <i class="tio-more-horizontal"></i>
                                            </a>

                                            <div id="settingsDropdown3" class="hs-unfold-content dropdown-unfold dropdown-menu dropdown-menu-right mt-1 hs-unfold-hidden hs-unfold-content-initialized hs-unfold-css-animation animated hs-unfold-reverse-y" data-hs-target-height="243" data-hs-unfold-content="" data-hs-unfold-content-animation-in="slideInUp" data-hs-unfold-content-animation-out="fadeOut" style="animation-duration: 300ms;">
                                                <span class="dropdown-header">Settings</span>

                                                <a class="dropdown-item" href="#">
                                                    <i class="tio-edit dropdown-item-icon"></i> Edit
                                                </a>
                                                <a class="dropdown-item" href="#">
                                                    <i class="tio-publish dropdown-item-icon"></i> Publish
                                                </a>
                                                <a class="dropdown-item" href="#">
                                                    <i class="tio-delete-outlined dropdown-item-icon"></i> Delete
                                                </a>

                                                <div class="dropdown-divider"></div>

                                                <span class="dropdown-header">Feedback</span>

                                                <a class="dropdown-item" href="#">
                                                    <i class="tio-reply dropdown-item-icon"></i> Reply
                                                </a>
                                            </div>
                                        </div>
                                        <!-- End Unfold -->
                                    </td>
                                </tr><tr role="row" class="even">
                                    <td class="table-column-pr-0">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="usersDataCheck5">
                                            <label class="custom-control-label" for="usersDataCheck5"></label>
                                        </div>
                                    </td>
                                    <th>
                                        <a class="d-flex align-items-center" href="./user-profile.html">
                                            <div class="avatar">
                                                <img class="avatar-img" src="./assets/img/400x400/img19.jpg" alt="Image Description">
                                            </div>
                                            <div class="ml-3">
                                                <span class="h5 text-hover-primary">Levis women's jeans</span>
                                            </div>
                                        </a>
                                    </th>
                                    <td>
                                        <a class="d-flex align-items-center" href="./user-profile.html">
                                            <div class="avatar avatar-circle">
                                                <img class="avatar-img" src="./assets/img/160x160/img5.jpg" alt="Image Description">
                                            </div>
                                            <div class="ml-3">
                                                <span class="d-block h5 text-hover-primary mb-0">David Harrison</span>
                                                <span class="d-block font-size-sm text-body">david@example.com</span>
                                            </div>
                                        </a>
                                    </td>
                                    <td>
                                        <div class="text-wrap" style="width: 18rem;">
                                            <div class="d-flex mb-2">
                                                <div class="mr-1"><img src="./assets/svg/components/star.svg" alt="Review rating" width="14"></div>
                                                <div class="mr-1"><img src="./assets/svg/components/star.svg" alt="Review rating" width="14"></div>
                                                <div class="mr-1"><img src="./assets/svg/components/star.svg" alt="Review rating" width="14"></div>
                                                <div class="mr-1"><img src="./assets/svg/components/star.svg" alt="Review rating" width="14"></div>
                                                <div class="mr-1"><img src="./assets/svg/components/star.svg" alt="Review rating" width="14"></div>
                                            </div>

                                            <h4 class="mb-1">Buy the product, you will not regret it!</h4>
                                            <p>Don't let this merchandise get away! It's a must buy and you will look good in it while working out.</p>
                                        </div>
                                    </td>
                                    <td>June 08, 2020, 07:22</td>
                                    <td><span class="badge badge-soft-success ml-2">Published</span></td>
                                    <td>
                                        <!-- Unfold -->
                                        <div class="hs-unfold">
                                            <a class="js-hs-unfold-invoker btn btn-icon btn-sm btn-ghost-secondary rounded-circle" href="javascript:;" data-hs-unfold-options="{
                           &quot;target&quot;: &quot;#settingsDropdown4&quot;,
                           &quot;type&quot;: &quot;css-animation&quot;
                         }" data-hs-unfold-target="#settingsDropdown4" data-hs-unfold-invoker="">
                                                <i class="tio-more-horizontal"></i>
                                            </a>

                                            <div id="settingsDropdown4" class="hs-unfold-content dropdown-unfold dropdown-menu dropdown-menu-right mt-1 hs-unfold-hidden hs-unfold-content-initialized hs-unfold-css-animation animated hs-unfold-reverse-y" data-hs-target-height="243" data-hs-unfold-content="" data-hs-unfold-content-animation-in="slideInUp" data-hs-unfold-content-animation-out="fadeOut" style="animation-duration: 300ms;">
                                                <span class="dropdown-header">Settings</span>

                                                <a class="dropdown-item" href="#">
                                                    <i class="tio-edit dropdown-item-icon"></i> Edit
                                                </a>
                                                <a class="dropdown-item" href="#">
                                                    <i class="tio-publish dropdown-item-icon"></i> Publish
                                                </a>
                                                <a class="dropdown-item" href="#">
                                                    <i class="tio-delete-outlined dropdown-item-icon"></i> Delete
                                                </a>

                                                <div class="dropdown-divider"></div>

                                                <span class="dropdown-header">Feedback</span>

                                                <a class="dropdown-item" href="#">
                                                    <i class="tio-reply dropdown-item-icon"></i> Reply
                                                </a>
                                            </div>
                                        </div>
                                        <!-- End Unfold -->
                                    </td>
                                </tr><tr role="row" class="odd">
                                    <td class="table-column-pr-0">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="usersDataCheck6">
                                            <label class="custom-control-label" for="usersDataCheck6"></label>
                                        </div>
                                    </td>
                                    <th>
                                        <a class="d-flex align-items-center" href="./user-profile.html">
                                            <div class="avatar">
                                                <img class="avatar-img" src="./assets/img/400x400/img11.jpg" alt="Image Description">
                                            </div>
                                            <div class="ml-3">
                                                <span class="h5 text-hover-primary">Asos t-shirts</span>
                                            </div>
                                        </a>
                                    </th>
                                    <td>
                                        <a class="d-flex align-items-center" href="./user-profile.html">
                                            <div class="avatar avatar-soft-dark avatar-circle">
                                                <span class="avatar-initials">B</span>
                                            </div>
                                            <div class="ml-3">
                                                <span class="d-block h5 text-hover-primary mb-0">Bob Dean</span>
                                                <span class="d-block font-size-sm text-body">bob@example.com</span>
                                            </div>
                                        </a>
                                    </td>
                                    <td>
                                        <div class="text-wrap" style="width: 18rem;">
                                            <div class="d-flex mb-2">
                                                <div class="mr-1"><img src="{{asset('/images/bitboss/icons/star.svg')}}" alt="Review rating" width="14"></div>

                                                <div class="mr-1"><img src="./assets/svg/components/star-muted.svg" alt="Review rating" width="14"></div>
                                            </div>

                                            <h4 class="mb-1">Ready for the heat!</h4>
                                            <p>As good as the heat Rdy T-shirt but without the sleeves. Love the stripes on the back.</p>
                                        </div>
                                    </td>
                                    <td>May 27, 2020, 04:01</td>
                                    <td><span class="badge badge-soft-warning ml-2">Pending</span></td>
                                    <td>
                                        <!-- Unfold -->
                                        <div class="hs-unfold">
                                            <a class="js-hs-unfold-invoker btn btn-icon btn-sm btn-ghost-secondary rounded-circle" href="javascript:;" data-hs-unfold-options="{
                           &quot;target&quot;: &quot;#settingsDropdown5&quot;,
                           &quot;type&quot;: &quot;css-animation&quot;
                         }" data-hs-unfold-target="#settingsDropdown5" data-hs-unfold-invoker="">
                                                <i class="tio-more-horizontal"></i>
                                            </a>

                                            <div id="settingsDropdown5" class="hs-unfold-content dropdown-unfold dropdown-menu dropdown-menu-right mt-1 hs-unfold-hidden hs-unfold-content-initialized hs-unfold-css-animation animated hs-unfold-reverse-y" data-hs-target-height="243" data-hs-unfold-content="" data-hs-unfold-content-animation-in="slideInUp" data-hs-unfold-content-animation-out="fadeOut" style="animation-duration: 300ms;">
                                                <span class="dropdown-header">Settings</span>

                                                <a class="dropdown-item" href="#">
                                                    <i class="tio-edit dropdown-item-icon"></i> Edit
                                                </a>
                                                <a class="dropdown-item" href="#">
                                                    <i class="tio-publish dropdown-item-icon"></i> Publish
                                                </a>
                                                <a class="dropdown-item" href="#">
                                                    <i class="tio-delete-outlined dropdown-item-icon"></i> Delete
                                                </a>

                                                <div class="dropdown-divider"></div>

                                                <span class="dropdown-header">Feedback</span>

                                                <a class="dropdown-item" href="#">
                                                    <i class="tio-reply dropdown-item-icon"></i> Reply
                                                </a>
                                            </div>
                                        </div>
                                        <!-- End Unfold -->
                                    </td>
                                </tr></tbody>
                            </table><div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Showing 1 to 5 of 7 entries</div></div>
                    </div>
                </div>
            </div>

            </div>
        </div>
    </div>

@endsection