@extends('admin.app', ['title'=>__($user->fullName)])

@section('content')

    <form action="{{route('admin.users.update', $user)}}" method="post">
        {!! csrf_field() !!}
        {{ method_field('PUT') }}
        <div>
            <div class="content container-fluid">
                <div class="page-header m-0">
                    <div class="row align-items-end">
                        <div class="col-sm mb-2 mb-sm-0">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb breadcrumb-no-gutter">
                                    <li class="breadcrumb-item"><a class="breadcrumb-link" href="{{route('admin.users.index')}}:;">Utenti</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">{{$user->fullName}}</li>
                                </ol>
                            </nav>

                            <h1 class="page-header-title">{{$user->fullName}}</h1>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">
                            Salva
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="mt-3">
            @include('admin.partials._errors_list')
        </div>

        <div class="tab-content pt-4">
            <div class="tab-pane show active" id="details" role="tabpanel">
                @include('admin.users.partials.form')
            </div>
        </div>
    </form>

@endsection