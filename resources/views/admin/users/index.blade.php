@extends('admin.app', ['title'=>__("Utenti")])

@section('content')

    <div class="mb-7">
        <div class="content container-fluid">
            <!-- Page Header -->
            <div class="page-header">
                <div class="row align-items-end">
                    <div class="col-sm mb-2 mb-sm-0">
                        {{--<nav aria-label="breadcrumb">
                            <ol class="breadcrumb breadcrumb-no-gutter">
                                <li class="breadcrumb-item"><a class="breadcrumb-link" href="javascript:;">Pages</a></li>
                                <li class="breadcrumb-item"><a class="breadcrumb-link" href="javascript:;">Users</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Overview</li>
                            </ol>
                        </nav>--}}

                        <h1 class="page-header-title">Utenti</h1>
                    </div>

                    <div class="col-sm-auto">
                        <a class="btn btn-primary" href="{{route('admin.users.create')}}">
                            <i class="tio-user-add mr-1"></i> Nuovo
                        </a>
                    </div>
                </div>
                <!-- End Row -->
            </div>
            <!-- End Page Header -->

            <div class="card">
                <div class="card-body">
                    <div class="table-responsive-sm">
                        <table class="table table-striped table-centered mb-0">
                            <thead class="thead-light">
                            <tr>
                                <th>Nome e cognome</th>
                                <th>Email</th>
                                <th>Ruolo</th>
                                <th>Azioni</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($users as $user)
                                <tr role="row" class="odd">
                                    <td class="table-column-pl-0">
                                        {{--<div class="avatar avatar-circle">
                                            <img class="avatar-img" src="./assets/img/160x160/img10.jpg" alt="Image Description">
                                        </div>--}}
                                        <div class="ml-3">
                                            <a class="d-flex align-items-center" href="{{route('admin.users.edit', $user)}}">
                                                <div class="avatar avatar-sm avatar-soft-primary avatar-circle mr-2">
                                                    <span class="avatar-initials">{{$user->first_name[0]}}</span>
                                                </div>
                                                <div class="ml-3">
                                                    <span class="d-block h5 text-hover-primary mb-0">{{$user->fullName}}</span>
                                                </div>
                                            </a>
                                        </div>
                                    </td>
                                    <td>
                                        <span class="d-block text-hover-primary mb-0">{{$user->email}}</span>
                                    </td>
                                    <td>
                                        <span class="d-block mb-0">@lang('users.roles.' . $user->role->name)</span>
                                    </td>
                                    <td>
                                        <a class="btn btn-xs btn-white" href="{{route('admin.users.edit', $user)}}">
                                            <i class="tio-edit"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            </div>
        </div>
    </div>

@endsection