<div class="row">
    <div class="col-lg-3">
        <div class="navbar-vertical navbar-expand-lg mb-3 mb-lg-5">
            <!-- Nav -->
            <div id="navbarVerticalNavMenu" class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-nav-lg nav-tabs card card-navbar-nav " role="tablist">
                    <li class="nav-item">
                        <a class="nav-link {{ !$errors->has('user.password_confirmation') && !$errors->has('user.password') ? 'active' : '' }}" data-toggle="tab"
                           href="#main"
                           role="tab" aria-controls="nav-one-eg1" aria-selected="true">
                            Info generali
                        </a>
                    </li>
                    @if(isset($user))
                        <li class="nav-item">
                            <a class="nav-link {{$errors->has('user.password_confirmation') || $errors->has('user.password') ? 'active' : '' }}" data-toggle="tab"
                               href="#password"
                               role="tab" aria-controls="nav-one-eg2" aria-selected="true">
                                Password
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
            <!-- End Nav -->
        </div>
    </div>
    <div class="col-lg-9">
        <div class="tab-content">
            {{-- ANAGRAFICA --}}
            <div class="tab-pane fade {{ !$errors->has('user.password_confirmation') && !$errors->has('user.password') ? 'active show' : '' }}" id="main" role="tabpanel">
                @include('admin.users.partials.main')
            </div>

            @if(isset($user))
                {{-- PASSWORD --}}
                <div class="tab-pane fade {{$errors->has('user.password_confirmation') || $errors->has('user.password') ? 'active show' : '' }}" id="password" role="tabpanel">
                    @include('admin.users.partials.password')
                </div>
            @endif
        </div>
    </div>

</div>