<div id="app">
    <div class="card">
        <div class="card-header custom-card-header">
            Utente
        </div>
        <div class="card-body">
            <div class="row">
                {{-- CODICE OFFERTA --}}

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="password" class="text-grey-50 font-14">{{__('Password')}}</label>
                        <input type="password" class="{{$errors->has('password') ? 'is-invalid form-control' : 'form-control'}}"
                        placeholder="Inserisci la tua password" name="password[password]">
                        {!! $errors->first('password', '<span class="invalid-feedback">:message</span>') !!}
                    </div>
                </div>
                {{-- CODICE OFFERTA --}}
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="password" class="text-grey-50 font-14">{{__('Conferma Password')}}</label>
                        <input type="password" class="form-control"
                               placeholder="Ripeti password" name="password[password_confirmation]">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>