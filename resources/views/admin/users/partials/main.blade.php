<div class="hidden" style="display: none;">
    <input type="hidden" id="obj_profile_image">
</div>
<div id="app">
    <div class="card">
        <div class="card-header custom-card-header">
            Utente
        </div>
        <div class="card-body">
            <div class="row">
                {{-- CODICE OFFERTA --}}
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Nome</label>
                        <input class="form-control" type="text" name="user[first_name]" value="{{isset($user) ? $user->first_name : ''}}">
                    </div>
                </div>
                {{-- CODICE OFFERTA --}}
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Cognome</label>
                        <input class="form-control" type="text" name="user[last_name]" value="{{isset($user) ? $user->last_name : ''}}">
                    </div>
                </div>
                {{-- CODICE OFFERTA --}}
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Email</label>
                        <input class="form-control" type="email" name="user[email]" value="{{isset($user) ? $user->email : ''}}">
                    </div>
                </div>
                {{--<div class="col-md-12">
                    <div class="form-group">
                    <label>Immagine profilo</label>
                        <vue-dropzone-upload
                                @if (isset($user) && $user->getMedia('profile_image')->count())
                                    :file="{{$user->getMedia('profile_image')->first()->toJson()}}"
                                    url-preview="{{$user->getMedia('profile_image')->first()->getFullUrl()}}"
                                @endif
                                propid="profile_image">
                        </vue-dropzone-upload>
                    </div>
                </div>--}}
            </div>
        </div>
    </div>
</div>