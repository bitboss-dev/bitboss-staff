@extends('auth.app', ['title'=>__("Login")])

@section('content')
    <div class="position-fixed top-0 right-0 left-0 bg-img-hero"
         style="height: 32rem; background-image: url({{asset('svg/components/abstract-bg-4.svg')}});">
        <!-- SVG Bottom Shape -->
        <figure class="position-absolute right-0 bottom-0 left-0">
            <svg preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 1921 273">
                <polygon fill="#fff" points="0,273 1921,273 1921,0 "/>
            </svg>
        </figure>
        <!-- End SVG Bottom Shape -->
    </div>

    <form method="POST" action="{{ route('login') }}">
        @csrf
    <!-- Content -->

    <!-- Card -->
    <div class="card card-lg mb-5">
        <div class="card-body">
            <!-- Form -->
            <form class="js-validate">
                <div class="text-center">
                    <div class="mb-5">
                        <h1 class="display-4">Login</h1>
                    </div>
                </div>

                <div class="js-form-message form-group">
                    <label class="input-label" for="signinSrEmail">Email</label>
                    <input type="email" class="form-control form-control-lg" name="email" id="signinSrEmail"
                           placeholder="email@esempio.com" aria-label="email@address.com" required
                           data-msg="Inserisci un'email valida.">
                    @if($errors->any())
                        <small class="text-danger">
                            @foreach ($errors->all() as $error)
                                {{$error}}
                            @endforeach
                        </small>
                    @endif
                </div>
                <!-- End Form Group -->

                <!-- Form Group -->
                <div class="js-form-message form-group">
                    <label class="input-label" for="signupSrPassword">
                            <span class="d-flex justify-content-between align-items-center">
                              Password
                              <a class="input-label-secondary" href="{{route('password.request')}}">Password dimenticata?</a>
                            </span>
                    </label>

                    <div class="input-group input-group-merge">
                        <input type="password" class="js-toggle-password form-control form-control-lg" name="password"
                               id="signupSrPassword" placeholder="Password" aria-label="8+ caratteri richiesti" required
                               data-msg="La tua password è errata, ritenta."
                               data-hs-toggle-password-options='{
                                     "target": "#changePassTarget",
                                     "defaultClass": "tio-hidden-outlined",
                                     "showClass": "tio-visible-outlined",
                                     "classChangeTarget": "#changePassIcon"
                                   }'>
                        {{--<div id="changePassTarget" class="input-group-append">
                            <a class="input-group-text" href="javascript:;">
                                <i id="changePassIcon" class="tio-visible-outlined"></i>
                            </a>
                        </div>--}}
                    </div>
                </div>
                <!-- End Form Group -->

                <!-- Checkbox -->
                <div class="form-group">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="termsCheckbox" name="termsCheckbox">
                        <label class="custom-control-label font-size-sm text-muted" for="termsCheckbox">
                            Ricordami</label>
                    </div>
                </div>
                <!-- End Checkbox -->

                <button type="submit" class="btn btn-lg btn-block btn-primary">Login</button>
            </form>
            <!-- End Form -->
        </div>
    </div>
    <!-- End Card -->

    </form>
    <!-- End Content -->
@endsection