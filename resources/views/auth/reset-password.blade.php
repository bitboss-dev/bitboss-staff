@extends('auth.app', ['title'=>__("Nuova Password")])
@section('content')


    <!-- Card -->
    <div class="card card-lg mb-5">
        <div class="card-body">

            <div class="text-center">
                <div class="mb-5">
                    <h1 class="display-4">Imposta Nuova Password</h1>
                    <p>Inserisci il tuo indirizzo email nuova password.</p>
                </div>
            </div>
            <!-- Form -->
            <form method="POST" action="{{ route('password.update') }}">
                @csrf
                <input type="hidden" name="token" value="{{ $request->route('token') }}">
                <div class="block">
                    <label for="email"></label>
                    <input type="email" class="form-control form-control-lg" name="email" id="email"
                           value="{{ $request->email }}" placeholder="Inserisci il tuo indirizzo email"
                    >
                    @error('email')
                    <div class="text-danger" role="alert">
                        <span>{{ $message }}</span>
                    </div>
                    @enderror
                </div>

                <div class="block mt-4">
                    <label for="password">{{ __('Password') }}</label>
                    <input type="password" class="form-control form-control-lg" name="password" id="password">
                    @error('password')
                    <div class="text-danger" role="alert">
                        <span>{{ $message }}</span>
                    </div>
                    @enderror
                </div>

                <div class="block mt-4">
                    <label for="password_confirmation">{{ __('Confirm Password') }}</label>
                    <input type="password" class="form-control form-control-lg" name="password_confirmation" id="password_confirmation">
                    @error('password_confirmation')
                    <div class="text-danger" role="alert">
                        <span>{{ $message }}</span>
                    </div>
                    @enderror
                </div>

                <div class="flex items-center justify-end mt-4">
                    <button type="submit" class="btn btn-lg btn-block btn-primary">{{ __('Reset Password') }}</button>
                </div>
            </form>
        </div>
    </div>

@endsection