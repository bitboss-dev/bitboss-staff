<!DOCTYPE html>

<html lang="en">
    <head>
        <!-- Required Meta Tags Always Come First -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Title -->
        <title>Bitboss Staff | Login</title>

        <!-- Favicon -->
        <link rel="shortcut icon" href="./favicon.ico">

        <!-- Font -->
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600&display=swap" rel="stylesheet">

        <!-- CSS Implementing Plugins -->
        <link rel="stylesheet" href="{{ asset('css/admin.app.css') }}">
    </head>

    <body>

        <main id="content" role="main" class="main">
            <div class="container py-5 py-sm-7">
                <a class="d-flex justify-content-center mb-5" href="index.html">
                    <img class="z-index-2 auth-brand-logo" src="{{asset('images/logo/logo.png')}}" alt="Image Description" style="width: 8rem;">
                </a>

                <div class="row justify-content-center">
                    <div class="col-md-7 col-lg-5">
                        @yield('content')

                        @include('auth.partials.footer')
                    </div>
                </div>
            </div>
        </main>

        <script src="{{ asset('js/admin.app.js') }}"></script>

        <script>
            if (/MSIE \d|Trident.*rv:/.test(navigator.userAgent)) document.write('<script src="./assets/vendor/babel-polyfill/polyfill.min.js"><\/script>');
        </script>
    </body>
</html>