@extends('auth.app', ['title'=>__("Password dimenticata")])

@section('content')
    <div class="position-fixed top-0 right-0 left-0 bg-img-hero"
         style="height: 32rem; background-image: url({{asset('svg/components/abstract-bg-4.svg')}});">
        <!-- SVG Bottom Shape -->
        <figure class="position-absolute right-0 bottom-0 left-0">
            <svg preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 1921 273">
                <polygon fill="#fff" points="0,273 1921,273 1921,0 "/>
            </svg>
        </figure>
        <!-- End SVG Bottom Shape -->
    </div>
    <!-- Card -->
    <div class="card card-lg mb-5">
        <div class="card-body">

            @if (session('status'))
                <div class="text-success text-center">
                    {{ session('status') }}
                </div>
            @else
                <div class="text-center">
                    <div class="mb-5">
                        <h1 class="display-4">Password dimenticata?</h1>
                        <p>Inserisci il tuo indirizzo email e ti invieremo istruzioni per il reset della password.</p>
                    </div>
                </div>
                <!-- Form -->
                <form method="POST" action="{{ route('password.email') }}" class="js-validate" novalidate="novalidate">
                @csrf
                <!-- Form Group -->
                    <div class="js-form-message form-group">
                        <label class="input-label" for="resetPasswordSrEmail" tabindex="0">Email</label>
                        <input type="email" class="form-control form-control-lg" name="email" id="resetPasswordSrEmail"
                               tabindex="1" placeholder="Inserisci il tuo indirizzo email"
                               aria-label="Enter your email address" required=""
                               data-msg="Please enter a valid email address.">

                        @error('email')
                        <div class="text-danger" role="alert">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <!-- End Form Group -->
                    <button type="submit" class="btn btn-lg btn-block btn-primary">Invia</button>
                </form>
                <!-- End Form -->
                <div class="text-center mt-2">
                    <a class="btn btn-link" href="{{route('login')}}">
                        <i class="tio-chevron-left"></i> Torna indietro
                    </a>
                </div>
            @endif
        </div>
    </div>

@endsection