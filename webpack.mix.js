const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/admin/admin.app.js', 'public/js')
    .vue()
    .sass('resources/sass/admin/admin.app.scss', 'public/css');

mix.js('resources/js/bitboss/bitboss.app.js', 'public/js')
    .vue()
    .sass('resources/sass/bitboss/bitboss.app.scss', 'public/css');

mix.copy('resources/themes/front/dist/assets/vendor/hs-unfold', 'public/js/vendor/hs-unfold');
mix.copy('resources/images', 'public/images');