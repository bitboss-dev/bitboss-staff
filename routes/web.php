<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', App\Http\Controllers\Auth\AuthRedirectorController::class);

Route::name('admin.')->prefix('admin')->middleware(['isAdmin', 'auth'])->group(function () {
    Route::get('/dashboard', \App\Http\Controllers\Web\Admin\DashboardController::class)->name('dashboard');

    /** Notifications */
        Route::get('/notifications/{notification}/read', '\App\Http\Controllers\Web\Admin\Notifications\NotificationController@read')->name('notifications.read');
        Route::get('/notifications/readAll', '\App\Http\Controllers\Web\Admin\Notifications\NotificationController@readAll')->name('notifications.readAll');

    /** Models */
        Route::resource('users', \App\Http\Controllers\Web\Admin\Users\UserController::class, ['except' => ['show']]);
        Route::resource('permits', \App\Http\Controllers\Web\Admin\Permits\PermitController::class, ['except' => ['show']]);
        Route::resource('reports', \App\Http\Controllers\Web\Admin\Reports\ReportController::class);
});

Route::name('bitboss.')->prefix('bitboss')->middleware(['isBitboss', 'auth'])->group(function () {
    Route::get('/home', \App\Http\Controllers\Web\Bitboss\HomeController::class)->name('home');

    /** Notifications */
        Route::get('/notifications/{notification}/read', '\App\Http\Controllers\Web\Bitboss\Notifications\NotificationController@read')->name('notifications.read');
        Route::get('/notifications/readAll', '\App\Http\Controllers\Web\Admin\Bitboss\NotificationController@readAll')->name('notifications.readAll');

    /** Models */
        Route::resource('permits', \App\Http\Controllers\Web\Bitboss\Permits\PermitController::class);
        Route::resource('reports', \App\Http\Controllers\Web\Bitboss\Reports\ReportController::class);

    /** Files */
        Route::post('uploads', '\App\Http\Controllers\Web\Bitboss\Reports\UploadController@upload')->name('upload');
});
