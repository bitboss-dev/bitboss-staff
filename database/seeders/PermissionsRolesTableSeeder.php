<?php

namespace Database\Seeders;

use App\Models\Users\Client;
use App\Models\Users\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionsRolesTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::create(['name' => 'Admin']);
        $bitboss = Role::create(['name' => 'Bitboss']);

        Permission::create(['name' => 'admin.users.show', 'display_name' => 'Vedere gli utenti', 'category' => 'Amministrazione']);
        Permission::create(['name' => 'admin.users.update', 'display_name' => 'Modificare gli utenti', 'category' => 'Amministrazione']);

        Permission::create(['name' => 'admin.permits.show', 'display_name' => 'Vedere i permessi', 'category' => 'Amministrazione']);
        Permission::create(['name' => 'admin.permits.update', 'display_name' => 'Modificare i permessi', 'category' => 'Amministrazione']);

        Permission::create(['name' => 'admin.reports.show', 'display_name' => 'Vedere le segnalazioni', 'category' => 'Amministrazione']);
        Permission::create(['name' => 'admin.reports.update', 'display_name' => 'Modificare le segnalazioni', 'category' => 'Amministrazione']);

        Permission::create(['name' => 'bitboss.permits.show', 'display_name' => 'Vedere i permessi', 'category' => 'Bitboss']);
        Permission::create(['name' => 'bitboss.permits.update', 'display_name' => 'Modificare i permessi', 'category' => 'Bitboss']);

        Permission::create(['name' => 'bitboss.reports.show', 'display_name' => 'Vedere le segnalazioni', 'category' => 'Bitboss']);
        Permission::create(['name' => 'bitboss.reports.update', 'display_name' => 'Modificare le segnalazioni', 'category' => 'Bitboss']);

        $admin->givePermissionTo('admin.users.show');
        $admin->givePermissionTo('admin.users.update');

        $admin->givePermissionTo('admin.permits.show');
        $admin->givePermissionTo('admin.permits.update');

        $admin->givePermissionTo('admin.reports.show');
        $admin->givePermissionTo('admin.reports.update');

        $admin->givePermissionTo('bitboss.permits.show');
        $admin->givePermissionTo('bitboss.permits.update');

        $bitboss->givePermissionTo('bitboss.permits.show');
        $bitboss->givePermissionTo('bitboss.permits.update');

        $bitboss->givePermissionTo('bitboss.reports.show');
        $bitboss->givePermissionTo('bitboss.reports.update');
    }
}
