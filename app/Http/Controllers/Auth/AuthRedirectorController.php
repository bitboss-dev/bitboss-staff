<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Support\Facades\Auth;

class AuthRedirectorController extends Controller
{
    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function __invoke()
    {
        if (Auth::guest()) {
            return view('auth.login');
        } else {
            if (Auth::user()->hasRole('Admin')) {
                return redirect(route('admin.dashboard'));
            } else if (Auth::user()->hasRole('Bitboss')) {
                return redirect(route('bitboss.home'));
            }
        }
    }
}
