<?php

namespace App\Http\Controllers\Web\Bitboss\Permits;

use App\Http\Controllers\Controller;
use App\Models\Permits\Permit;

class PermitController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $this->authorize('bitboss.permits.show');

        return view('bitboss.permits.index');
    }
}
