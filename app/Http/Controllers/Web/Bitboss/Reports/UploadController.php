<?php

namespace App\Http\Controllers\Web\Bitboss\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UploadController extends Controller
{
    public function upload(Request $request)
    {
        $files = $request->allFiles();
        $uploadedFiles = [];
        $directory = $request->directory;
        $tmpPath = storage_path('app/public/uploads');
        $path = 'uploads/';

        foreach ($files as $fieldName => $file) {
            $fileName = Str::slug($file->getClientOriginalName())."-".(uniqid()).".".$file->extension();
            $file->move($tmpPath, $fileName);
            $uploadedFiles[] = [
                'field_name' => $fieldName,
                'path' => $path,
                'name' => $fileName,
                'size' => $file->getSize(),
                'type' => $file->getClientMimeType(),
                'url' => asset('storage/uploads/' . $fileName)
            ];
        }

        return $uploadedFiles;


    }
}
