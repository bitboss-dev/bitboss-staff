<?php

namespace App\Http\Controllers\Web\Bitboss\Reports;

use App\Http\Controllers\Controller;
use App\Models\Permits\Permit;
use App\Models\Reports\Report;
use App\Models\User;

class ReportController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        $this->authorize('bitboss.reports.show');

        $reports = Report::all();
        $users = User::all();

        return view('bitboss.reports.create', compact('reports', 'users'));
    }
}
