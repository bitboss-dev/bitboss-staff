<?php

namespace App\Http\Controllers\Web\Admin\Permits;

use App\Http\Controllers\Controller;
use App\Models\Permits\Permit;

class PermitController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $this->authorize('admin.permits.show');

        $permits = Permit::getForCalendar();

        return view('admin.permits.index', compact('permits'));
    }
}
