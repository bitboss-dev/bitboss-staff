<?php

namespace App\Http\Controllers\Web\Admin\Users;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Validators\Admin\Users\UsersValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UserController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $this->authorize('admin.users.show');

        $users = User::all();

        return view('admin.users.index', compact('users'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        $this->authorize('admin.users.update');

        return view('admin.users.create');
    }

    /**
     * @param Request $request
     * @param UsersValidator $validator
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request, UsersValidator $validator)
    {
        $this->authorize('admin.users.update');

        $validator->validate_store($request);

        $user = User::create(
            array_merge($request->user, [
                'password' => bcrypt(Str::random(10)),
            ]));

        $user->assignRole('Bitboss');

        //Invio email

        return redirect(route('admin.users.index'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(User $user)
    {
        $this->authorize('admin.users.update');

        return view('admin.users.edit', compact('user'));
    }

    /**
     * @param Request $request
     * @param User $user
     * @param UsersValidator $validator
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, User $user, UsersValidator $validator)
    {
        $this->authorize('admin.users.update');

        $validator->validate_update($request);

        $user->update($request->user);

        if ($request->password)
            $user->update(['password' => bcrypt($request->password['password'])]);

        return redirect(route('admin.users.index'));
    }
}
