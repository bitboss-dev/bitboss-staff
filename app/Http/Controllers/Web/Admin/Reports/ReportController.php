<?php

namespace App\Http\Controllers\Web\Admin\Reports;

use App\Http\Controllers\Controller;
use App\Models\Permits\Permit;
use App\Models\Reports\Report;
use App\Models\User;

class ReportController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $this->authorize('admin.reports.show');

        $reports = Report::all();
        $users = User::all();

        return view('admin.reports.index', compact('reports', 'users'));
    }
}
