<?php

namespace App\Http\Controllers\Web\Admin\Notifications;

use App\Http\Controllers\Controller;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{

    /**
     * @param DatabaseNotification $notification
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function read(DatabaseNotification $notification)
    {
        if (!$notification->read_at) {
            $notification->markAsRead();
        }

        return redirect($notification->data['redirect']);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function readAll()
    {
        foreach (Auth::user()->notifications as $notification) {
            if (!$notification->read_at) {
                $notification->markAsRead();
            }
        }

        return back();
    }
}
