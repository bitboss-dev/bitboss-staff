<?php

namespace App\Validators\Admin\Users;

use App\Events\QuotePaid;
use App\Http\Controllers\Controller;
use App\Models\Client\Quotes\Quote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UsersValidator extends Controller
{
    /**
     * @param Request $request
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validate_store(Request $request)
    {
        $messages = [
            'user.first_name.required' => 'Il nome è obbligatorio',
            'user.last_name.required' => 'Il cognome è obbligatorio',
            'user.email.required' => 'L\'email è obbligatoria',
            'user.email.unique' => 'L\'email è associata ad un altro utente',
        ];

        $this->validate($request, [
            'user.first_name' => 'required',
            'user.last_name' => 'required',
            'user.email' => 'required|email|unique:users,email',
        ], $messages);
    }

    /**
     * @param Request $request
     */
    public function validate_update(Request $request)
    {
        if ($request->password['password']) {
            $this->validate_passwords($request);
        }

        $messages = [
            'user.first_name.required' => 'Il nome è obbligatorio',
            'user.last_name.required' => 'Il cognome è obbligatorio',
            'user.email.required' => 'L\'email è obbligatoria',
        ];

        $this->validate($request, [
            'user.first_name' => 'required',
            'user.last_name' => 'required',
            'user.email' => 'required|email',
        ], $messages);
    }

    /**
     * @param Request $request
     */
    public function validate_passwords(Request $request)
    {
        $messages = [
            'password.password_confirmation.required' => 'Inserisci due password uguali',
            'password.password.confirmed' => 'Conferma la tua nuova password',
            'password.password.min' => 'La password deve essere lunga almeno 6 caratteri',
            'password.password_confirmation.same' => 'Le password devono coincidere',
        ];

        $this->validate($request, [
            'password.password' => 'min:6|confirmed',
            'password.password_confirmation' => 'same:password.password|required',
        ], $messages);
    }
}
