<?php

namespace App\Traits\Permits;

use App\Models\Permits\Permit;

trait PermitsOnCalendarTrait
{
    /**
     * @return array
     */
    public static function getForCalendar()
    {
        $permits = Permit::all();
        $permitsForCalendar = [];

        foreach ($permits as $permit) {
            $permitForCalendar = [];
            $customData = [];
            $popover = [];
            $dates = [];

            $customData['title'] = $permit->reason;
            $customData['author'] = 'Di ' . $permit->user->fullName;
            $customData['class'] = 'text-white ' . ($permit->approved ? 'bg-teal-500' : 'bg-red-600');
            $popover['visibility'] = 'click';
            $dates['start'] = $permit->from;
            $dates['end'] = $permit->to;

            $permitForCalendar['key'] = $permit->id;
            $permitForCalendar['customData'] = $customData;
            $permitForCalendar['popover'] = $popover;
            $permitForCalendar['dates'] = $dates;

            $permitsForCalendar[] = ($permitForCalendar);
        }

        return $permitsForCalendar;
    }
}
