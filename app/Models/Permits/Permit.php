<?php

namespace App\Models\Permits;

use App\Models\User;
use App\Traits\Permits\PermitsOnCalendarTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Permit extends Authenticatable
{
    use PermitsOnCalendarTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
