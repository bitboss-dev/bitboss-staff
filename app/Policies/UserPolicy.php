<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * CompanyPolicy constructor.
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function dashboard(User $user)
    {
        return $user->hasRole('Admin');
    }
}